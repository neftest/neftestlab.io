﻿using NefTest.Common.Attributes;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Tests {
    [TestCategory]
    public class EngineTests {
        [TestCase]
        public void Some_Test_That_Should_Pass() {
            1.ShouldBePositive();
        }
        [TestCase]
        public void Some_Other_Test() {
            1.ShouldBePositive();
        }
        [TestCase]
        public void A_Test_That_Does_Things() {
            1.ShouldBePositive();
        }
        [TestCase]
        public void Make_Sure_That_Thing_Works() {
            1.ShouldBePositive();
        }
        [TestCase]
        public void Do_More_Tests() {
            1.ShouldBePositive();
        }
        [TestCase]
        public void Even_More_Tests_Are_Here() {
            1.ShouldBePositive();
        }
        [TestCase]
        public void Even_More_Tests_Are_Here_THIS_FAILS_THOUGH() {
            1.ShouldBeNegative();
        }
    }
}
