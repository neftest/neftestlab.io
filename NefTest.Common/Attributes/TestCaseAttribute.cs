﻿using NefTest.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Common.Attributes {
    public class TestCaseAttribute : Attribute {
        public EnvironmentType EnvironmentType { get; }

        public TestCaseAttribute(EnvironmentType environmentType = EnvironmentType.Any) {
            EnvironmentType = environmentType;
        }
    }
}
