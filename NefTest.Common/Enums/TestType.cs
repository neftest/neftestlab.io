﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Common.Enums {
    [Flags]
    public enum TestType {
        Unit = 0x0001,
        E2E = 0x0002,

        All = 0xFFFF
    }
}
