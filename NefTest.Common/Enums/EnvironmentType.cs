﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Common.Enums {
    [Flags]
    public enum EnvironmentType {
        None              = 0x0000,

        Console           = 0x0001,
        ACClient          = 0x1000,
        DecalContainer    = 0x2000,

        Any               = 0xFFFF
    }
}
