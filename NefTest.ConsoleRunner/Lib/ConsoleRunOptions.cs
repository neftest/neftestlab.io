﻿using CommandLine;
using NefTest.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.ConsoleRunner.Lib {
    public class RunOptions {
        [Option('v', "verbose", DefaultValue = false, HelpText = "Enables verbose output.")]
        public bool Verbose { get; set; }

        [Option('t', "test-types", DefaultValue = TestType.Unit, HelpText = "Types of tests to run. Available types are Unit, and E2E.")]
        public TestType TestTypes { get; set; }

        [Option('e', "test-env", DefaultValue = EnvironmentType.Console, HelpText = "The environment to run the tests inside. Available environments are Console, ACClient, and DecalContainer.")]
        public EnvironmentType EnvironmentType { get; set; }

        [ValueList(typeof(List<string>))]
        public List<string> TestFiles { get; set; } = new List<string>();
    }
}
