﻿using NefTest.Engine;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NefTest.ConsoleRunner.Lib {
    public class ConsoleTestFinder : ITestFinder {
        private RunOptions _runOptions;
        private IEngine _engine;

        public ConsoleTestFinder(RunOptions runOptions, IEngine engine) {
            _runOptions = runOptions;
            _engine = engine;
        }

        public IEnumerable<ITestAssembly> FindAllAvailableTestAssemblies() {
            return _runOptions.TestFiles
                .Select(assemblyPath => {
                    return _engine.Resolve<ITestAssembly>(new Dictionary<object, object>() {
                        { "assemblyPath", assemblyPath }
                    });
                });
        }

        public ITestAssembly GetTestAssembly(string assemblyPath) {
            throw new System.NotImplementedException();
        }

        public ITestCase GetTestCase(string assemblyPath, string testCaseName) {
            throw new System.NotImplementedException();
        }
    }
}
