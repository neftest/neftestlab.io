﻿using NefTest.Common.Enums;
using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using NefTest.Engine.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.ConsoleRunner.Lib {
    public class ConsoleTestRunner : ITestRunner {
        private readonly IEngine _engine;
        private readonly ITestFinder _testFinder;
        private readonly ILogger _logger;

        public ConsoleTestRunner(IEngine engine, ITestFinder testFinder, ILogger logger) {
            _engine = engine;
            _testFinder = testFinder;
            _logger = logger;
        }

        public ITestResult RunAvailableTests(TestFilter filter) {
            var testResult = _engine.Resolve<ITestResult>();
            testResult.Filter = filter;

            foreach (var testAssembly in _testFinder.FindAllAvailableTestAssemblies()) {
                testAssembly.Load();

                var testAssemblyResult = _engine.Resolve<ITestAssemblyResult>(new Dictionary<object, object> {
                    { "fullyQualifiedName", testAssembly.Assembly.FullName },
                    { "shortName", testAssembly.Assembly.GetName().Name },
                    { typeof(ITestAssembly), testAssembly }
                });
                testResult.AddAssemblyResult(testAssemblyResult);

                // this assembly resolver should probably move somewhere else...
                var handler = new ResolveEventHandler(testAssembly.LoadAssemblyBytesFromTestDirectory);
                AppDomain.CurrentDomain.AssemblyResolve += handler;

                try {
                    var testResults = new List<ITestCaseResult>();
                    foreach (var testClass in testAssembly.GetTestableClasses()) {
                        var testClassResult = _engine.Resolve<ITestClassResult>(new Dictionary<object, object> {
                            { "fullyQualifiedName", testClass.TestClassType.FullName },
                            { "shortName", testClass.TestClassType.Name },
                            { typeof(ITestClass), testClass }
                        });
                        testAssemblyResult.AddClassResult(testClassResult);
                        var classInstance = testClass.CreateTestableInstance();

                        foreach (var testCase in testClass.GetTestCases()) {
                            if (filter.Matches(testAssembly) && filter.Matches(testClass) && filter.Matches(testCase)) {
                                var testCaseResult = testCase.Run(classInstance);
                                testClassResult.AddCaseResult(testCaseResult);
                            }
                            else {
                                var missingEnvs = filter.GetMissingEnvironments(new List<IFilterable>() { testAssembly, testClass, testCase });
                                var testCaseResult = _engine.Resolve<ITestCaseResult>(new Dictionary<object, object> {
                                    { "fullyQualifiedName", testCase.FullyQualifiedName },
                                    { "shortName", testCase.MethodInfo.Name },
                                    { typeof(TestStatus), TestStatus.NotRun },
                                    { typeof(TimeSpan), TimeSpan.Zero },
                                    { "output", $"Needs Environment: {missingEnvs}" },
                                    { typeof(ITestCase), testCase }
                                });
                                testClassResult.AddCaseResult(testCaseResult);
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    _logger.Log(ex);
                }
                finally {
                    AppDomain.CurrentDomain.AssemblyResolve -= handler;
                }
            }

            return testResult;
        }

        public ITestResult FindAvailableTests(TestFilter filter) {
            var testResult = _engine.Resolve<ITestResult>();
            testResult.Filter = filter;

            foreach (var testAssembly in _testFinder.FindAllAvailableTestAssemblies()) {
                testAssembly.Load();

                var testAssemblyResult = _engine.Resolve<ITestAssemblyResult>(new Dictionary<object, object> {
                    { "fullyQualifiedName", testAssembly.Assembly.FullName },
                    { "shortName", testAssembly.Assembly.GetName().Name },
                    { typeof(ITestAssembly), testAssembly }
                });
                testResult.AddAssemblyResult(testAssemblyResult);

                // this assembly resolver should probably move somewhere else...
                var handler = new ResolveEventHandler(testAssembly.LoadAssemblyBytesFromTestDirectory);
                AppDomain.CurrentDomain.AssemblyResolve += handler;

                try {
                    var testResults = new List<ITestCaseResult>();
                    foreach (var testClass in testAssembly.GetTestableClasses()) {
                        var testClassResult = _engine.Resolve<ITestClassResult>(new Dictionary<object, object> {
                            { "fullyQualifiedName", testClass.TestClassType.FullName },
                            { "shortName", testClass.TestClassType.Name },
                            { typeof(ITestClass), testClass }
                        });
                        testAssemblyResult.AddClassResult(testClassResult);
                        var classInstance = testClass.CreateTestableInstance();

                        foreach (var testCase in testClass.GetTestCases()) {
                            var testCaseResult = _engine.Resolve<ITestCaseResult>(new Dictionary<object, object> {
                                    { "fullyQualifiedName", testCase.FullyQualifiedName },
                                    { "shortName", testCase.MethodInfo.Name },
                                    { typeof(TestStatus), TestStatus.NotRun },
                                    { typeof(TimeSpan), TimeSpan.Zero },
                                    { "output", $"" },
                                    { typeof(ITestCase), testCase }
                                });
                            testClassResult.AddCaseResult(testCaseResult);
                        }
                    }
                }
                catch (Exception ex) {
                    _logger.Log(ex);
                }
                finally {
                    AppDomain.CurrentDomain.AssemblyResolve -= handler;
                }
            }

            return testResult;
        }
    }
}
