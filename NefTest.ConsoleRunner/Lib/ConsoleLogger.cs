﻿using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;

namespace NefTest.ConsoleRunner.Lib {
    public class ConsoleLogger : ILogger {
        private const int STD_OUTPUT_HANDLE = -11;
        private const uint ENABLE_VIRTUAL_TERMINAL_PROCESSING = 0x0004;
        private const uint DISABLE_NEWLINE_AUTO_RETURN = 0x0008;

        private Dictionary<string, int> Colors = new Dictionary<string, int>() {
            { "black", 30 },
            { "red", 31 },
            { "green", 32 },
            { "yellow", 33 },
            { "blue", 34 },
            { "magenta", 35 },
            { "cyan", 36 },
            { "white", 37 },
            { "grey", 90 },
            { "lightred", 91 },
            { "lightgreen", 92 },
            { "lightyellow", 93 },
            { "lightblue", 94 },
            { "lightmagenta", 95 },
            { "lightcyan", 96 },
            { "lightwhite", 97 },
            { "bold", 1 },
            { "faint", 2 },
            { "underline", 4 },
        };

        [DllImport("kernel32.dll")]
        private static extern bool GetConsoleMode(IntPtr hConsoleHandle, out uint lpMode);

        [DllImport("kernel32.dll")]
        private static extern bool SetConsoleMode(IntPtr hConsoleHandle, uint dwMode);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern IntPtr GetStdHandle(int nStdHandle);

        [DllImport("kernel32.dll")]
        public static extern uint GetLastError();

        private RunOptions _runOptions;

        public ConsoleLogger(RunOptions runOptions) {
            _runOptions = runOptions;
            TryEnableAnsiConsoleColors();
        }

        private void TryEnableAnsiConsoleColors() {
            try {
                var iStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
                if (GetConsoleMode(iStdOut, out uint outConsoleMode)) {
                    outConsoleMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING | DISABLE_NEWLINE_AUTO_RETURN;
                    SetConsoleMode(iStdOut, outConsoleMode);
                }
            }
            catch { }
        }

        public void Log(string message, LogLevel logLevel = LogLevel.Info) {
            Console.ResetColor();
            
            switch (logLevel) {
                case LogLevel.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
            }

            if (_runOptions.Verbose || !logLevel.Equals(LogLevel.Verbose))
                Console.WriteLine(Colorize(Regex.Replace(message, @"\r?\n", "\r\n")));
        }

        public void Log(Exception ex) {
            Console.WriteLine($"Exception: {Regex.Replace(ex.ToString(), @"\r?\n", "\r\n")}");
        }

        private string Colorize(string message) {
            var colorized = message;

            foreach (var kv in Colors) {
                colorized = colorized.Replace($"[{kv.Key}]", $"\u001b[{kv.Value}m");
                colorized = colorized.Replace($"[/{kv.Key}]", $"\u001b[0m");
            }

            return colorized;
        }
    }
}
