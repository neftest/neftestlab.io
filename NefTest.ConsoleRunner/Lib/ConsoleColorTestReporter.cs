﻿using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace NefTest.ConsoleRunner.Lib {
    public class ConsoleColorTestReporter : ITestReporter {
        private readonly ILogger _logger;

        public ConsoleColorTestReporter(ILogger logger) {
            _logger = logger;
        }

        public void Write(ITestResult testResult) {
            _logger.Log(BuildStatusString(0, $"Test Results (Env: {testResult.Filter.Environment})", testResult));

            foreach (var testAssembly in testResult.GetAssemblyResults()) {
                _logger.Log(BuildStatusString(1, testAssembly.ShortName, testAssembly));

                foreach (var testClass in testAssembly.GetClassResults()) {
                    _logger.Log(BuildStatusString(2, testClass.ShortName, testClass));

                    foreach (var testCase in testClass.GetCaseResults()) {
                        StringBuilder testCaseStatusBuilder = new StringBuilder(200);
                        testCaseStatusBuilder.Append($"      - {ColorStatus(testCase.ShortName, testCase.Status)}: ");
                        testCaseStatusBuilder.Append($"{ColorStatus(testCase.Status.ToString(), testCase.Status, true)}");

                        if (testCase.Status == TestStatus.Failed && testCase.Output != null) {
                            testCaseStatusBuilder.Append($" ([magenta]{testCase.Duration.TotalMilliseconds:N3}ms[/magenta])");
                            testCaseStatusBuilder.Append($": \n[lightred]{testCase.Output}[/lightred]");
                        }
                        else if (testCase.Status == TestStatus.NotRun && testCase.Output != null) {
                            testCaseStatusBuilder.Append($": [lightyellow]{testCase.Output}[/lightyellow]");
                            testCaseStatusBuilder.Append($" ([magenta]{testCase.Duration.TotalMilliseconds:N3}ms[/magenta])");
                        }
                        else {
                            testCaseStatusBuilder.Append($" ([magenta]{testCase.Duration.TotalMilliseconds:N3}ms[/magenta])");
                        }

                        _logger.Log(testCaseStatusBuilder.ToString());
                    }
                }
            }
        }

        private string BuildStatusString(int indentLevel, string title, IReportable result) {
            var indent = "".PadRight(indentLevel * 2, ' ');
            var failing = result.TotalCasesByStatus(TestStatus.Failed);
            var passing = result.TotalCasesByStatus(TestStatus.Passed);
            var notRun = result.TotalCasesByStatus(TestStatus.NotRun);
            var status = (failing > 0) ? TestStatus.Failed : (notRun > 0 ? TestStatus.NotRun : TestStatus.Passed);

            StringBuilder statusBuilder = new StringBuilder(200);
            statusBuilder.Append($"{indent}- {ColorStatus(title, status)}: ");
            statusBuilder.Append($"{ColorStatus($"{failing} failing", failing > 0 ? TestStatus.Failed : TestStatus.Invalid, (failing > 0))}, ");
            statusBuilder.Append($"{ColorStatus($"{passing} passing", passing > 0 ? TestStatus.Passed : TestStatus.Invalid, (passing > 0))}, ");
            statusBuilder.Append($"{ColorStatus($"{notRun} not run", notRun > 0 ? TestStatus.NotRun : TestStatus.Invalid, (notRun > 0))}, ");
            statusBuilder.Append($"{result.TotalCases()} total ([magenta]{result.Duration.TotalMilliseconds:N3}ms[/magenta])");
            return statusBuilder.ToString();
        }

        private string ColorStatus(string message, TestStatus status, bool useLightColors=false) {
            if (useLightColors) {
                switch (status) {
                    case TestStatus.Passed:
                        return $"[lightgreen]{message}[/lightgreen]";
                    case TestStatus.Failed:
                        return $"[lightred]{message}[/lightred]";
                    case TestStatus.NotRun:
                        return $"[lightyellow]{message}[/lightyellow]";
                    default:
                        return $"[white]{message}[/white]";
                }
            }
            else {
                switch (status) {
                    case TestStatus.Passed:
                        return $"[green]{message}[/green]";
                    case TestStatus.Failed:
                        return $"[red]{message}[/red]";
                    case TestStatus.NotRun:
                        return $"[yellow]{message}[/yellow]";
                    default:
                        return $"[grey]{message}[/grey]";
                }
            }
        }
    }
}
