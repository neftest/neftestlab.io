﻿using NefTest.Common.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using CommandLine;
using NefTest.Engine;
using NefTest.Engine.Interfaces;
using NefTest.Engine.Enums;
using NefTest.ConsoleRunner.Lib;
using NefTest.Engine.Lib;

namespace NefTest.ConsoleRunner {
    static class Program {
        static void Main(string[] args) {
            var runOptions = ParseCommandLineOptions(args);
            var engine = new NefTestEngine();

            engine.RegisterComponent(typeof(RunOptions), typeof(RunOptions), true, runOptions);
            engine.RegisterComponent(typeof(ILogger), typeof(ConsoleLogger), true);
            engine.RegisterComponent(typeof(ITestFinder), typeof(ConsoleTestFinder), true);
            engine.RegisterComponent(typeof(ITestRunner), typeof(ConsoleTestRunner), true);
            engine.RegisterComponent(typeof(ITestReporter), typeof(ConsoleColorTestReporter), true);

            engine.Initialize();

            engine.TryStartServer(26463);

            var filter = new TestFilter() {
                Environment = runOptions.EnvironmentType
            };
            var testResult = engine.TestRunner.RunAvailableTests(filter);

            engine.TestReporter.Write(testResult);
        }

        private static RunOptions ParseCommandLineOptions(string[] args) {
            var runOptions = new RunOptions();
            var parser = new Parser((settings) => {
                settings.CaseSensitive = true;
                settings.IgnoreUnknownArguments = false;
                settings.MutuallyExclusive = false;
                settings.HelpWriter = Console.Error;
            });

            parser.ParseArgumentsStrict(args, runOptions, ShowExtendedHelp);

            return runOptions;
        }

        private static void ShowExtendedHelp() {
            Console.WriteLine($"Test Environments:");
            Console.WriteLine();
            Console.WriteLine($"This console runner is capable of running tests in different environments.");
            Console.WriteLine($"You can use the --test-env <env> option to specify the runner environment.");
            Console.WriteLine($"Available options are listed below:");
            Console.WriteLine();
            Console.WriteLine($" - Console");
            Console.WriteLine($"     - Runs the tests inside this executable. Good for Unit tests.");
            Console.WriteLine();
            Console.WriteLine($" - ACClient");
            Console.WriteLine($"     - Starts an acclient instance to run tests inside. Good for E2E tests.");
            Console.WriteLine();
            Console.WriteLine($" - DecalContainer");
            Console.WriteLine("      - Runs tests inside of DecalContainer.exe. Good for E2E UI tests.");
            Console.WriteLine();
            Console.WriteLine("Usage Examples:");
            Console.WriteLine();
            Console.WriteLine("  # Run End2End tests contained in ./tests/NeftTest.Plugin.Tests.dll");
            Console.WriteLine("  ./NefTest.ConsoleRunner.exe -t E2E ./tests/NeftTest.Plugin.Tests.dll");
            Console.WriteLine();
            Console.WriteLine("  # Run All tests matching paths ./*.Tests.dll or ./tests/*.Tests.dll");
            Console.WriteLine("  ./NefTest.ConsoleRunner.exe -t All");
            Console.WriteLine();
            Console.WriteLine("  # Run Unit tests contained in ./NefTest.Tests.dll and ./Adapter.Tests.dll");
            Console.WriteLine("  ./NefTest.ConsoleRunner.exe ./NefTest.Tests.dll ./Adapter.Tests.dll");
            Console.WriteLine();
            Console.WriteLine("  # Run all available tests in an actual ac client");
            Console.WriteLine("  ./NefTest.ConsoleRunner.exe -test-types All -test-env ACClient");
            Console.WriteLine();
            Console.WriteLine("Notes:");
            Console.WriteLine();
            Console.WriteLine("  If no test assembly arguments are specified, it will attempt to load all");
            Console.WriteLine("  tests matching the wildcard paths ./*.Tests.dll or ./tests/*.Tests.dll");
            Console.WriteLine();
            Environment.Exit(1);
        }
    }
}
