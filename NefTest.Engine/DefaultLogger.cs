﻿using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine {
    public class DefaultLogger : ILogger {
        public void Log(string message, LogLevel logLevel = LogLevel.Info) {
            Console.WriteLine($"{logLevel}: {message}");
        }

        public void Log(Exception ex) {
            Console.WriteLine($"Exception: {ex}");
        }
    }
}
