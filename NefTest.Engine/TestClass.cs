﻿using NefTest.Common.Attributes;
using NefTest.Common.Enums;
using NefTest.Engine.Interfaces;
using NefTest.Engine.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace NefTest.Engine {
    /// <summary>
    /// The default implementation of ITestClass.
    /// </summary>
    public class TestClass : ITestClass {
        /// <summary>
        /// The ITestAssembly that this test class is defined in.
        /// </summary>
        public ITestAssembly TestAssembly { get; set; }

        /// <summary>
        /// The Type of the test class.
        /// </summary>
        public Type TestClassType { get; set; }

        public EnvironmentType Environment {
            get {
                var thisEnv = ((TestCategoryAttribute)TestClassType.GetCustomAttributes(typeof(TestCategoryAttribute), true).First()).EnvironmentType;
                return (thisEnv == EnvironmentType.Any) ? TestAssembly.Environment : thisEnv;
            }
        }

        private readonly BindingFlags _methodBindingFlags = BindingFlags.Public| BindingFlags.Instance | BindingFlags.DeclaredOnly;
        private readonly IEngine _engine;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="engine">Active IEngine instance</param>
        /// <param name="testAssembly">The ITestAssembly this test class is defined in.</param>
        /// <param name="testClassType">The Type of the test class.</param>
        public TestClass(IEngine engine, ITestAssembly testAssembly, Type testClassType) {
            _engine = engine;
            TestAssembly = testAssembly;
            TestClassType = testClassType;
        }

        /// <summary>
        /// Create an instance of the testable class.
        /// </summary>
        /// <returns>A new instance of TestClassType.</returns>
        public object CreateTestableInstance() {
            return Activator.CreateInstance(TestClassType);
        }

        /// <summary>
        /// Get all the ITestCase's contained within the test class.
        /// </summary>
        /// <returns>A list of available ITestCases</returns>
        public IEnumerable<ITestCase> GetTestCases() {
            return TestClassType
                .GetMethods(_methodBindingFlags)
                .Where(Utils.IsTestCaseMethod)
                .Select(methodInfo => {
                    return _engine.Resolve<ITestCase>(new Dictionary<object, object>() {
                        { typeof(MethodInfo), methodInfo },
                        { typeof(ITestClass), this }
                    });
                });
        }
    }
}
