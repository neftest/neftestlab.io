﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Enums {
    public enum TestStatus {
        /// <summary>
        /// The test was run and succeeded.
        /// </summary>
        Passed = 0,

        /// <summary>
        /// The test was run and failed.
        /// </summary>
        Failed = 1,

        /// <summary>
        /// The test was not run, probably because it was just being discovered.
        /// </summary>
        NotRun = 2,

        /// <summary>
        /// The test was skipped, probably because the TestFilter did not match
        /// </summary>
        Skipped = 3,

        /// <summary>
        /// Invalid
        /// </summary>
        Invalid = 99
    };
}
