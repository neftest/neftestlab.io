﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Enums {
    public enum LogLevel {
        Verbose = 0,
        Info = 1,
        Warning = 2,
        Error = 3,
        Success = 4
    }
}
