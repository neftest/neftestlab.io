﻿using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine {
    public class LoggerTestReporter : ITestReporter {
        private ILogger _logger;

        public LoggerTestReporter(ILogger logger) {
            _logger = logger;
        }

        public void Write(ITestResult testResult) {
            int totalFailing = testResult.TotalCasesByStatus(TestStatus.Failed);
            int totalPassing = testResult.TotalCasesByStatus(TestStatus.Passed);
            int totalNotRun = testResult.TotalCasesByStatus(TestStatus.NotRun);
            int total = testResult.GetAssemblyResults().Sum(a => a.GetClassResults().Sum(c => c.GetCaseResults().Count()));

            _logger.Log($"Test Results: {totalFailing} failing, {totalPassing} passing, {totalNotRun} not run, {total} total ({testResult.Duration.TotalMilliseconds:N3}ms)");

            foreach (var testAssembly in testResult.GetAssemblyResults()) {
                int assemblyFailing = testAssembly.TotalCasesByStatus(TestStatus.Failed);
                int assemblyPassing = testAssembly.TotalCasesByStatus(TestStatus.Passed);
                int assemblyNotRun = testAssembly.TotalCasesByStatus(TestStatus.NotRun);
                _logger.Log($"  - {testAssembly.ShortName}: {assemblyFailing} failing, {assemblyPassing} passing, {assemblyNotRun} not run ({testAssembly.Duration.TotalMilliseconds:N3}ms)");

                foreach (var testClass in testAssembly.GetClassResults()) {
                    int testClassFailing = testClass.TotalCasesByStatus(TestStatus.Failed);
                    int testClassPassing = testClass.TotalCasesByStatus(TestStatus.Passed);
                    int testClassNotRun = testClass.TotalCasesByStatus(TestStatus.NotRun);
                    _logger.Log($"    - {testClass.ShortName}: {testClassFailing} failing, {testClassPassing} passing, {testClassNotRun} not run ({testClass.Duration.TotalMilliseconds:N3}ms)");

                    foreach (var testCase in testClass.GetCaseResults()) {
                        var output = string.IsNullOrEmpty(testCase.Output) ? "" : $": {testCase.Output}";
                        _logger.Log($"      - {testCase.ShortName}: {testCase.Status}{output} ({testCase.Duration.TotalMilliseconds:N3}ms)");
                    }
                }
            }
        }
    }
}
