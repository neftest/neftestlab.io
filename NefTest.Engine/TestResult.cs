﻿using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using NefTest.Engine.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine {
    public class TestResult : ITestResult {
        private readonly List<ITestAssemblyResult> _results = new List<ITestAssemblyResult>();

        public TimeSpan Duration => new TimeSpan(_results.Sum(r => r.Duration.Ticks));

        public TestFilter Filter { get; set; } = new TestFilter();

        public TestStatus Status {
            get {
                if (TotalCasesByStatus(TestStatus.Failed) > 0)
                    return TestStatus.Failed;
                else if (TotalCasesByStatus(TestStatus.NotRun) > 0)
                    return TestStatus.NotRun;
                else if (TotalCasesByStatus(TestStatus.Skipped) > 0)
                    return TestStatus.Skipped;
                else if (TotalCasesByStatus(TestStatus.Passed) > 0)
                    return TestStatus.Passed;
                else
                    return TestStatus.NotRun;
            }
        }

        public void AddAssemblyResult(ITestAssemblyResult result) {
            _results.Add(result);
        }

        public IEnumerable<ITestAssemblyResult> GetAssemblyResults() {
            return _results;
        }

        public int TotalCases() {
            return _results.Sum(a => a.TotalCases());
        }

        public int TotalCasesByStatus(TestStatus status) {
            return _results.Sum(a => a.TotalCasesByStatus(status));
        }
    }
}
