﻿using NefTest.Common.Enums;
using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using NefTest.Engine.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine {
    public class DefaultTestRunner : ITestRunner {
        private readonly IEngine _engine;
        private readonly ITestFinder _testFinder;
        private readonly ILogger _logger;

        public DefaultTestRunner(IEngine engine, ITestFinder testFinder, ILogger logger) {
            _engine = engine;
            _testFinder = testFinder;
            _logger = logger;
        }

        public ITestResult RunAvailableTests(TestFilter filter) {
            var testResult = _engine.Resolve<ITestResult>();
            testResult.Filter = filter;
            IEnumerable<ITestAssembly> testAssemblies;

            if (filter.AssemblyFilters.Count() > 0) {
                testAssemblies = filter.AssemblyFilters.Select(a => _testFinder.GetTestAssembly(a));
            }
            else {
                testAssemblies = _testFinder.FindAllAvailableTestAssemblies();
            }

            foreach (var testAssembly in testAssemblies) {
                testAssembly.Load();

                var testAssemblyResult = _engine.Resolve<ITestAssemblyResult>(new Dictionary<object, object> {
                    { "fullyQualifiedName", testAssembly.Assembly.FullName },
                    { "shortName", testAssembly.Assembly.GetName().Name },
                    { typeof(ITestAssembly), testAssembly }
                });
                testResult.AddAssemblyResult(testAssemblyResult);

                // this assembly resolver should probably move somewhere else...
                var handler = new ResolveEventHandler(testAssembly.LoadAssemblyBytesFromTestDirectory);
                AppDomain.CurrentDomain.AssemblyResolve += handler;

                try {
                    var testResults = new List<ITestCaseResult>();
                    foreach (var testClass in testAssembly.GetTestableClasses()) {
                        if (filter.Matches(testClass)) {
                            var testClassResult = _engine.Resolve<ITestClassResult>(new Dictionary<object, object> {
                                { "fullyQualifiedName", testClass.TestClassType.FullName },
                                { "shortName", testClass.TestClassType.Name },
                                { typeof(ITestClass), testClass }
                            });

                            testAssemblyResult.AddClassResult(testClassResult);
                            var classInstance = testClass.CreateTestableInstance();

                            foreach (var testCase in testClass.GetTestCases()) {
                                if (filter.Matches(testCase)) {
                                    var testCaseResult = testCase.Run(classInstance);
                                    testClassResult.AddCaseResult(testCaseResult);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    _logger.Log(ex);
                }
                finally {
                    AppDomain.CurrentDomain.AssemblyResolve -= handler;
                }
            }

            return testResult;
        }

        public ITestResult FindAvailableTests(TestFilter filter) {
            var testResult = _engine.Resolve<ITestResult>();
            testResult.Filter = filter;
            IEnumerable<ITestAssembly> testAssemblies;

            if (filter.AssemblyFilters.Count() > 0) {
                testAssemblies = filter.AssemblyFilters.Select(a => _testFinder.GetTestAssembly(a));
            }
            else {
                testAssemblies = _testFinder.FindAllAvailableTestAssemblies();
            }

            foreach (var testAssembly in testAssemblies) {
                testAssembly.Load();

                var testAssemblyResult = _engine.Resolve<ITestAssemblyResult>(new Dictionary<object, object> {
                    { "fullyQualifiedName", testAssembly.Assembly.FullName },
                    { "shortName", testAssembly.Assembly.GetName().Name },
                    { typeof(ITestAssembly), testAssembly }
                });
                testResult.AddAssemblyResult(testAssemblyResult);

                // this assembly resolver should probably move somewhere else...
                var handler = new ResolveEventHandler(testAssembly.LoadAssemblyBytesFromTestDirectory);
                AppDomain.CurrentDomain.AssemblyResolve += handler;

                try {
                    var testResults = new List<ITestCaseResult>();
                    foreach (var testClass in testAssembly.GetTestableClasses()) {
                        if (filter.Matches(testClass)) {
                            var testClassResult = _engine.Resolve<ITestClassResult>(new Dictionary<object, object> {
                                { "fullyQualifiedName", testClass.TestClassType.FullName },
                                { "shortName", testClass.TestClassType.Name },
                                { typeof(ITestClass), testClass }
                            });

                            testAssemblyResult.AddClassResult(testClassResult);
                            var classInstance = testClass.CreateTestableInstance();

                            foreach (var testCase in testClass.GetTestCases()) {
                                if (filter.Matches(testCase)) {
                                    var testCaseResult = _engine.Resolve<ITestCaseResult>(new Dictionary<object, object> {
                                        { "fullyQualifiedName", testCase.FullyQualifiedName },
                                        { "shortName", testCase.MethodInfo.Name },
                                        { typeof(TestStatus), TestStatus.NotRun },
                                        { typeof(TimeSpan), TimeSpan.Zero },
                                        { "output", null },
                                        { typeof(ITestCase), testCase }
                                    });
                                    testClassResult.AddCaseResult(testCaseResult);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex) {
                    _logger.Log(ex);
                }
                finally {
                    AppDomain.CurrentDomain.AssemblyResolve -= handler;
                }
            }

            return testResult;
        }
    }
}
