﻿using NefTest.Common.Enums;
using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using NefTest.Engine.Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NefTest.Engine {
    public class TestAssembly : ITestAssembly {
        public string AssemblyPath { get; set; }
        public Assembly Assembly { get; set; }
        public bool IsLoaded => _isLoaded;

        private readonly ILogger _log;
        private readonly IEngine _engine;
        private bool _isLoaded = false;

        public EnvironmentType Environment => EnvironmentType.Any;

        public TestAssembly(string assemblyPath, ILogger logger, IEngine engine) {
            AssemblyPath = assemblyPath;
            _log = logger;
            _engine = engine;
        }

        public bool Load() {
            if (_isLoaded)
                return true;

            if (!File.Exists(AssemblyPath)) {
                _log.Log($"Unable to find assembly: {AssemblyPath}", LogLevel.Error);
                return false;
            }

            var handler = new ResolveEventHandler(LoadAssemblyBytesFromTestDirectory);
            AppDomain.CurrentDomain.AssemblyResolve += handler;

            try {
                var possiblePdbPath = AssemblyPath.Replace(".dll", ".pdb");

                if (File.Exists(possiblePdbPath)) {
                    Assembly = Assembly.Load(File.ReadAllBytes(AssemblyPath), File.ReadAllBytes(possiblePdbPath));
                }
                else {
                    Assembly = Assembly.Load(File.ReadAllBytes(AssemblyPath));
                }

                // this is just an access check, will throw an exception if assembly cant be used from some reason
                // like its a newer framework version
                Assembly.GetTypes();
            }
            catch (Exception ex) {
                _log.Log($"Unable to load plugin test assembly: {AssemblyPath}", LogLevel.Error);
                _log.Log(ex);
                return false;
            }
            finally {
                AppDomain.CurrentDomain.AssemblyResolve -= handler;
            }

            _isLoaded = true;
            return true;
        }

        IEnumerable<ITestClass> ITestAssembly.GetTestableClasses() {
            try {
                return Assembly
                    .GetTypes()
                    .Where(Utils.IsTestableClass)
                    .Select(testClassType => {
                        return _engine.Resolve<ITestClass>(new Dictionary<object, object>() {
                        { typeof(ITestAssembly), this },
                        { typeof(Type), testClassType }
                        });
                    });
            }
            catch (Exception ex) {
                _log.Log($"Unable to load assembly types from {Assembly.FullName}: {ex}", LogLevel.Error);
            }
            return new List<ITestClass>();
        }

        public Assembly LoadAssemblyBytesFromTestDirectory(object sender, ResolveEventArgs args) {
            var dllName = new AssemblyName(args.Name).Name + ".dll";
            _log.Log($"Load PluginTestAssembly Dependency: {Assembly.GetName()} DEPENDS ON {dllName}", LogLevel.Verbose);

            //foreach (var a in AppDomain.CurrentDomain.GetAssemblies()) {
            //    if (a.GetName().Equals(args.Name))
            //        return a;
            //}

            string folderPath = Path.GetDirectoryName(AssemblyPath);
            string assemblyPath = Path.Combine(folderPath, new AssemblyName(args.Name).Name + ".dll");
            if (!File.Exists(assemblyPath)) {
                _log.Log($"Could not find PluginTestAssembly Dependency: {Assembly.GetName()} DEPENDS ON {assemblyPath}", LogLevel.Error);
                return null;
            }

            return Assembly.Load(File.ReadAllBytes(assemblyPath));
        }
    }
}
