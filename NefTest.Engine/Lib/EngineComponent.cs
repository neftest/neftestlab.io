﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Lib {
    internal class EngineComponent {
        public Type RegisteredType { get; set; }
        public Type ImplementedType { get; set; }
        public object Instance { get; set; }
        public bool IsSingleton { get; set; }
    }
}
