﻿using NefTest.Common.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NefTest.Engine.Lib {
    public class TestFilter {
        public EnvironmentType Environment { get; set; } = EnvironmentType.Any;

        public IEnumerable<string> AssemblyFilters { get; set; } = new List<string>();
        public IEnumerable<string> TestCaseFilters { get; set; } = new List<string>();

        public bool Matches(IFilterable filterable) {
            if (filterable.Environment != EnvironmentType.Any && Environment != EnvironmentType.Any) {
                var wantedFlags = filterable.Environment.ToString().Split(',').Select(flag => (EnvironmentType)Enum.Parse(typeof(EnvironmentType), flag)).ToList();
                var hasValidFlag = false;
                foreach (var wantedFlag in wantedFlags) {
                    if ((Environment & wantedFlag) != 0) {
                        hasValidFlag = true;
                        break;
                    }
                }

                if (!hasValidFlag) {
                    return false;
                }
            }

            if (filterable is ITestCase testCase) {
                if (TestCaseFilters.Count() > 0) {
                    var hasCaseMatch = false;
                    foreach (var testCaseFilter in TestCaseFilters) {
                        if (testCase.FullyQualifiedName.Equals(testCaseFilter)) {
                            hasCaseMatch = true;
                            break;
                        }
                    }

                    if (!hasCaseMatch) {
                        return false;
                    }
                }
            }
            else if (filterable is ITestAssembly testAssembly) {
                if (AssemblyFilters.Count() > 0) {
                    var hasAssemblyMatch = false;
                    foreach (var assemblyFilter in AssemblyFilters) {
                        if (testAssembly.AssemblyPath.ToLower().Equals(assemblyFilter.ToLower())) {
                            hasAssemblyMatch = true;
                            break;
                        }
                    }

                    if (!hasAssemblyMatch) {
                        return false;
                    }
                }
            }

            return true;
        }

        public EnvironmentType GetMissingEnvironments(IEnumerable<IFilterable> filterables) {
            foreach (var filterable in filterables) {
                if (filterable.Environment == EnvironmentType.Any || Environment == EnvironmentType.Any)
                    continue;

                var wantedFlags = filterable.Environment.ToString().Split(',').Select(flag => (EnvironmentType)Enum.Parse(typeof(EnvironmentType), flag)).ToList();

                foreach (var wantedFlag in wantedFlags) {
                    if ((Environment & wantedFlag) == 0)
                        return wantedFlag;
                }
            }

            return EnvironmentType.None;
        }
    }
}