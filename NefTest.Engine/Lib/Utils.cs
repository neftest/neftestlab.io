﻿using NefTest.Common.Attributes;
using NefTest.Common.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NefTest.Engine.Lib {
    /// <summary>
    /// A collection of various helper methods needed by the engine/runner/whatever.
    /// </summary>
    public static class Utils {
        /// <summary>
        /// Checks if the specified classType is a testable class type. Testable classes must be public,
        /// and have the [[TestCategory](xref:NefTest.Common.Attributes.TestCategoryAttribute)] Attribute.
        /// </summary>
        /// <param name="classType">The class type to check.</param>
        /// <returns>True if the class is testable, false otherwise.</returns>
        public static bool IsTestableClass(Type classType) {
            return classType.IsClass
                && classType.IsPublic
                && classType.GetCustomAttributes(typeof(TestCategoryAttribute), true).Length > 0;
        }

        /// <summary>
        /// Checks if the specified methodInfo is a test case method. Test case methods must be public,
        /// and have the [[TestCase](xref:NefTest.Common.Attributes.TestCaseAttribute)] Attribute.
        /// </summary>
        /// <param name="methodInfo">The methodInfo to check.</param>
        /// <returns>True if the method is a test case, false otherwise.</returns>
        public static bool IsTestCaseMethod(MethodInfo methodInfo) {
            return methodInfo.IsPublic
                && methodInfo.GetCustomAttributes(typeof(TestCaseAttribute), true).Length > 0;
        }
    }
}
