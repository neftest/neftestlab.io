﻿using NefTest.Engine.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Interfaces {
    public interface ITestAssemblyResult : IReportable {
        string FullyQualifiedName { get; }
        string ShortName { get; }
        ITestAssembly TestAssembly { get; }

        IEnumerable<ITestClassResult> GetClassResults();

        void AddClassResult(ITestClassResult result);

        bool IsPassing();
    }
}
