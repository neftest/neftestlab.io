﻿using NefTest.Engine.Enums;
using NefTest.Engine.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Interfaces {
    public interface ITestResult : IReportable {
        TestFilter Filter { get; set; }

        IEnumerable<ITestAssemblyResult> GetAssemblyResults();

        void AddAssemblyResult(ITestAssemblyResult result);
    }
}
