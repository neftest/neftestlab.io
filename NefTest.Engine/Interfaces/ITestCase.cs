﻿using System;
using System.Reflection;

namespace NefTest.Engine.Interfaces {
    /// <summary>
    /// Represents a specific test case. That is a single method with the 
    /// [[TestCase](xref:NefTest.Common.Attributes.TestCaseAttribute)] attribute contained
    /// inside of a valid [ITestClass](xref:NefTest.Engine.Interfaces.ITestClass).
    /// </summary>
    public interface ITestCase : IFilterable {
        string FullyQualifiedName { get; }
        MethodInfo MethodInfo { get; }
        ITestClass TestClass { get; }

        /// <summary>
        /// Run the test case.
        /// </summary>
        /// <param name="testClassInstance">An instance of the test class containing this ITestCase.</param>
        /// <returns>ITestResult containing test case results.</returns>
        ITestCaseResult Run(object testClassInstance);
    }
}