﻿using NefTest.Engine.Enums;
using System;

namespace NefTest.Engine.Interfaces {
    public interface IReportable {
        TimeSpan Duration { get; }

        TestStatus Status { get; }

        int TotalCasesByStatus(TestStatus status);

        int TotalCases();
    }
}