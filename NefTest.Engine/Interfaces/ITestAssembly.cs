﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NefTest.Engine.Interfaces {
    public interface ITestAssembly : IFilterable {
        string AssemblyPath { get; set; }
        Assembly Assembly { get; set; }
        bool IsLoaded { get; }

        bool Load();

        IEnumerable<ITestClass> GetTestableClasses();

        Assembly LoadAssemblyBytesFromTestDirectory(object sender, ResolveEventArgs args);
    }
}
