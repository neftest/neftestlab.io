﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Interfaces {
    /// <summary>
    /// The engine functions mostly as an IoC container that other NefTest components will use to resolve dependencies.
    /// 
    /// If you want to override certain interface implentations in NefTest this is where it is done. Just call the
    /// RegisterComponent method with your own interface implementation, and it will override the default registrations.
    /// </summary>
    public interface IEngine {
        ITestRunner TestRunner { get; }
        ITestFinder TestFinder { get; }
        ITestReporter TestReporter { get; }

        void Initialize();

        /// <summary>
        /// Registers the implementedType as a registerType component. This allows it to be resolved by the engine later.
        /// </summary>
        /// <param name="registerType">The type to register in the container as</param>
        /// <param name="implementedType">The implementation type used (must implement registerType interface)</param>
        /// <param name="isSingleton">Wether or not this class should be treated as a singleton.</param>
        /// <param name="instance">The instance to be used as the singleton. Setting this causes isSingleton param to be ignored.</param>
        void RegisterComponent(Type registerType, Type implementedType, bool isSingleton = false, object instance = null);

        /// <summary>
        /// Unregister the specified component type.
        /// </summary>
        /// <param name="registerType">The type to unregister</param>
        void UnregisterComponent(Type registerType);

        /// <summary>
        /// Resolve a type to an instance. This may return a singleton or a new instance depending on
        /// how the component was registered.
        /// </summary>
        /// <typeparam name="T">The type to resolve.</typeparam>
        /// <returns>The resolved instance of T.</returns>
        T Resolve<T>() where T : class;

        /// <summary>
        /// Resolve a type to an instance. Constructor params specified in paramOverloads will take
        /// precendence and be used instead of the internal Resolver. To use a named parameter overload
        /// add a KeyValuePair&lt;string, object&gt; where string is the parameter name as defined by the constructor.
        /// For typed parameter overloads use KeyValuePair&lt;Type, object&gt; where Type is the constructor parameter
        /// type you want to supply. The Resolver uses the following resolution order when creating a new instance:
        /// NamedParameterOverloads -&gt; TypedParameterOverloads -&gt; Resolve&lt;T&gt;(). This method always returns a new
        /// instance.
        /// </summary>
        /// <typeparam name="T">The type to resolve.</typeparam>
        /// <param name="parameterOverloads">Named/Typed parameters to be used in instantiation.</param>
        /// <returns>A new instance of T</returns>
        T Resolve<T>(IDictionary<object, object> parameterOverloads) where T : class;

        bool TryStartServer(int port);
    }
}
