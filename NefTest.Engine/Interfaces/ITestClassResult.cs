﻿using NefTest.Engine.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Interfaces {
    public interface ITestClassResult : IReportable {
        string FullyQualifiedName { get; }

        string ShortName { get; }
        ITestClass TestClass { get; }

        IEnumerable<ITestCaseResult> GetCaseResults();

        void AddCaseResult(ITestCaseResult result);

        bool IsPassing();
    }
}
