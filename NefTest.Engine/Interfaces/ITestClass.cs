﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace NefTest.Engine.Interfaces {
    /// <summary>
    /// Represents a class with tests defined. These are classes with the
    /// [[TestCategory](xref:NefTest.Common.Attributes.TestCategoryAttribute)] attribute defined.
    /// </summary>
    public interface ITestClass : IFilterable {
        /// <summary>
        /// The ITestAssembly that defines this test class.
        /// </summary>
        ITestAssembly TestAssembly { get; set; }

        /// <summary>
        /// The Type of the test class.
        /// </summary>
        Type TestClassType { get; set; }

        /// <summary>
        /// Creates a new instance of Type TestClassType.
        /// </summary>
        /// <returns>A newly created TestClassType instance.</returns>
        object CreateTestableInstance();

        /// <summary>
        /// Gets all the ITestCase's defined in this test class.
        /// </summary>
        /// <returns>A list of available test cases.</returns>
        IEnumerable<ITestCase> GetTestCases();
    }
}