﻿using NefTest.Common.Enums;
using NefTest.Engine.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine.Interfaces {
    public interface IFilterable {
        EnvironmentType Environment { get; }
    }
}
