﻿using NefTest.Engine.Enums;
using System;

namespace NefTest.Engine.Interfaces {
    public interface ITestCaseResult : IReportable {
        string FullyQualifiedName { get; }

        string ShortName { get; }
        
        string Output { get; }
        ITestCase TestCase { get; }
    }
}