﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NefTest.Engine.Interfaces {
    public interface ITestFinder {
        IEnumerable<ITestAssembly> FindAllAvailableTestAssemblies();

        ITestAssembly GetTestAssembly(string assemblyPath);

        ITestCase GetTestCase(string assemblyPath, string testCaseName);
    }
}
