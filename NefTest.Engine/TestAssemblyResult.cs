﻿using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.Engine {
    public class TestAssemblyResult : ITestAssemblyResult {
        private List<ITestClassResult> _results = new List<ITestClassResult>();

        public string FullyQualifiedName { get; }

        public string ShortName { get; }

        public ITestAssembly TestAssembly { get; }

        public TimeSpan Duration => new TimeSpan(_results.Sum(r => r.Duration.Ticks));

        public TestStatus Status {
            get {
                if (TotalCasesByStatus(TestStatus.Failed) > 0)
                    return TestStatus.Failed;
                else if (TotalCasesByStatus(TestStatus.NotRun) > 0)
                    return TestStatus.NotRun;
                else if (TotalCasesByStatus(TestStatus.Skipped) > 0)
                    return TestStatus.Skipped;
                else if (TotalCasesByStatus(TestStatus.Passed) > 0)
                    return TestStatus.Passed;
                else
                    return TestStatus.NotRun;
            }
        }

        public TestAssemblyResult(string fullyQualifiedName, string shortName, ITestAssembly testAssembly) {
            FullyQualifiedName = fullyQualifiedName;
            ShortName = shortName;
            TestAssembly = testAssembly;
        }

        public void AddClassResult(ITestClassResult result) {
            _results.Add(result);
        }

        public IEnumerable<ITestClassResult> GetClassResults() {
            return _results;
        }

        public bool IsPassing() {
            return _results.All(c => c.IsPassing());
        }

        public int TotalCasesByStatus(TestStatus status) {
            return _results.Sum(c => c.TotalCasesByStatus(status));
        }

        public int TotalCases() {
            return _results.Sum(a => a.TotalCases());
        }
    }
}
