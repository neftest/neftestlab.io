# Introduction to NefTest Console Runner

The NefTest Console Runner is a command-line utility for running unit and e2e tests. It can run on any windows machine with dotnet framework 3.5 installed.

The console runner can be used to automatically run and report test results in a continuous integration / delivery environment.

![NefTest Plugin Runner](/images/console-test-runner.png)

## Console Runner Exit Codes

If all tests pass, the console runner will exit with a code of `0`. If there are failing tests, the number of failing tests will be returned as the exit code.

| Exit Code | Description |
|---|---|
|   0   | All tests passed. |
|   >0  | A number of tests have failed.  |
|  -1   | Invalid command-line arguments. |
|  -2   | No tests were found. |
| -100  | An unexpected error occurred. |