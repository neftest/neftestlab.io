# Installing

The console runner can be installed from various sources. Detailed instructions are listed below for the different distribution channels.

## Using the Installer

- Download the latest NefTest.ConsoleRunner installer from the [Releases](https://gitlab.com/neftest/neftest.gitlab.io/-/releases) page.
- (Optional) Add NefTest.ConsoleRunner.exe to your environment `$PATH`.

## Using [Chocolatey](https://chocolatey.org/)

```
choco install neftest.consolerunner
```

After installing with [Chocolatey](https://chocolatey.org/) NefTest.ConsoleRunner.exe will automatically be added to your path.