# Running Tests

Tests can be run from the command-line, specifying the environment and types of tests to run.

## Options
| Option | Default | Description |
|---|---|---|
|  -v, --verbose     | False   | Enables verbose output. |
|  -t, --test-types  | Unit    |Types of tests to run. Available types are Unit, and E2E. |
|  -e, --test-env    | Console | The environment to run the tests inside. Available environments are Console, ACClient, and DecalContainer. |

## Test Types

| Test Type | Description |
|---|---|
| Unit  | Unit Tests. Should be able to run standalone in any environment. |
| E2E   | End to End Tests. Will run inside an actual AC Client. |

## Test Environments

The console runner is capable of running tests in different environments.
You can use the `--test-env <env>` option to specify the runner environment.
Available options are listed below:

| Environment | Description |
|---|---|
| Console        | Runs the tests inside this executable. Good for Unit tests. |
| ACClient       | Starts an acclient instance to run tests inside. Good for E2E tests. |
| DecalContainer | Runs tests inside of DecalContainer.exe. Good for E2E UI tests. |

## Usage Examples

Run End2End tests contained in ./tests/NeftTest.Plugin.Tests.dll

```
  ./NefTest.ConsoleRunner.exe -t E2E ./tests/NeftTest.Plugin.Tests.dll
```

Run All tests matching paths ./*.Tests.dll or ./tests/*.Tests.dll
```
  ./NefTest.ConsoleRunner.exe -t All
```

Run Unit tests contained in ./NefTest.Tests.dll and ./Adapter.Tests.dll
```
  ./NefTest.ConsoleRunner.exe ./NefTest.Tests.dll ./Adapter.Tests.dll
```

Run all available tests in an actual ac client
```
  ./NefTest.ConsoleRunner.exe -test-types All -test-env ACClient
```

## Notes

If no test assembly arguments are specified, the console runner will attempt to load all tests matching the wildcard paths ./*.Tests.dll or ./tests/*.Tests.dll in the current working directory.

