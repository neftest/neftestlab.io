﻿using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Threading;

namespace NefTest.PluginRunner.Tests {
    internal class ChatWatcher : IDisposable {

        public List<string> ChatLines { get; } = new List<string>();

        public ChatWatcher() {
            CoreManager.Current.ChatBoxMessage += Current_ChatBoxMessage;
        }

        private void Current_ChatBoxMessage(object sender, ChatTextInterceptEventArgs e) {
            CoreManager.Current.Actions.AddChatText($"I Saw: {e.Text}", 1);
            ChatLines.Add(e.Text);
        }

        public void Dispose() {
            CoreManager.Current.ChatBoxMessage -= Current_ChatBoxMessage;
        }
    }
}