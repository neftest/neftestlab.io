﻿using NefTest.Common.Attributes;
using NefTest.Common.Enums;
using NefTest.PluginRunner.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NefTest.PluginRunner.Tests.Unit {
    [TestCategory]
    public class PluginLoggerTests {

        [TestCase]
        public void LogCallsWriter() {
            var logger = new PluginLogger();
            logger.Log("test");
        }

        [TestCase(EnvironmentType.DecalContainer)]
        public void LogExceptionCallsWriter() {

        }
    }
}
