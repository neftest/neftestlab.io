﻿using Decal.Adapter;
using NefTest.Common.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Shouldly;
using NefTest.Common.Enums;

namespace NefTest.PluginRunner.Tests.E2E {
    [TestCategory(EnvironmentType.ACClient)]
    public class SanityTests {
        [TestCase(EnvironmentType.Any)]
        public void Always_Passes() {
            1.ShouldBe(1);
        }

        [TestCase]
        public void AppDomain_IsDefault() {
            AppDomain.CurrentDomain.FriendlyName.ShouldBe("DefaultDomain"); 
        }

        [TestCase]
        public void Decal_CoreManager_Actions_Exist() {
            CoreManager.Current.ShouldNotBeNull();
            CoreManager.Current.Actions.ShouldNotBeNull();
        }

        [TestCase]
        public void Decal_Interopt_Works() {
            var me = CoreManager.Current.CharacterFilter.Id;
            CoreManager.Current.Actions.SelectItem(me);
            CoreManager.Current.Actions.CurrentSelection.ShouldBe(me);
        }

        [TestCase]
        public void Test_That_Should_Fail() {
            CoreManager.Current.Actions.SelectItem(0);
            CoreManager.Current.Actions.CurrentSelection.ShouldBe(1);
        }
    }
}
