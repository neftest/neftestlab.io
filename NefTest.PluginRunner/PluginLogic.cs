﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using NefTest.Common.Enums;
using NefTest.Engine;
using NefTest.Engine.Interfaces;
using NefTest.Engine.Lib;
using NefTest.PluginRunner.Lib;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using VirindiViewService;
using VirindiViewService.Controls;
using VirindiViewService.XMLParsers;
using static NefTest.PluginRunner.Lib.KeyValueTreeView;

namespace NefTest.PluginRunner {
    /// <summary>
    /// This is where all your plugin logic should go.  Public fields are automatically serialized and deserialized
    /// between plugin sessions in this class.  Check out the main Plugin class to see how the serialization works.
    /// </summary>
    public class PluginLogic {
        // public fields will be serialized and restored between plugin sessions
        public int Counter = 0;

        [System.Xml.Serialization.XmlIgnoreAttribute]
        public NetServiceHost Host { get; private set; }

        [System.Xml.Serialization.XmlIgnoreAttribute]
        public CoreManager Core { get; private set; }

        // ignore a specific public field
        [System.Xml.Serialization.XmlIgnoreAttribute]
        public string PluginAssemblyDirectory;

        [System.Xml.Serialization.XmlIgnoreAttribute]
        public string AccountName { get; private set; }

        [System.Xml.Serialization.XmlIgnoreAttribute]
        public string CharacterName { get; private set; }

        [System.Xml.Serialization.XmlIgnoreAttribute]
        public string ServerName { get; private set; }

        // references to our view stuff is kept private so it is not serialized.
        private HudView view;
        private ViewProperties properties;
        private ControlGroup controls;

        private HudButton FindTestsButton;
        private HudButton RunTestsButton;
        private HudFixedLayout TestResultsLayout;
        private HudStaticText TestStatusText;
        private KeyValueTreeView TestResultsTreeView;
        private NefTestEngine _engine;

        #region Startup / Shutdown
        /// <summary>
        /// Called once when the plugin is loaded
        /// </summary>
        public void Startup(NetServiceHost host, CoreManager core, string pluginAssemblyDirectory, string accountName, string characterName, string serverName) {
            Host = host;
            Core = core;
            PluginAssemblyDirectory = pluginAssemblyDirectory;
            AccountName = accountName;
            CharacterName = characterName;
            ServerName = serverName;
            CreateView();
        }

        /// <summary>
        /// Called when the plugin is shutting down.  Unregister from any events here and do any cleanup. 
        /// </summary>
        public void Shutdown() {
            FindTestsButton.Hit -= FindTestsButton_Hit;
            RunTestsButton.Hit -= RunTestsButton_Hit;

            view.Visible = false;
            view.Dispose();
        }
        #endregion

        #region VVS Views
        /// <summary>
        /// Create our VVS view from an xml template.  We also assign references to the ui elements, as well 
        /// as register event handlers.
        /// </summary>
        private void CreateView() {
            new Decal3XMLParser().ParseFromResource("NefTest.PluginRunner.Views.MainView.xml", out properties, out controls);

            // main plugin view
            view = new HudView(properties, controls);

            view.UserResizeable = true;

            FindTestsButton = (HudButton)view["FindTestsButton"];
            RunTestsButton = (HudButton)view["RunTestsButton"];
            TestResultsLayout = (HudFixedLayout)view["TestResultsLayout"];
            TestStatusText = (HudStaticText)view["TestStatusText"];

            // ui event handlers
            FindTestsButton.Hit += FindTestsButton_Hit;
            RunTestsButton.Hit += RunTestsButton_Hit;

            // treeview to hold test results
            TestResultsTreeView = new KeyValueTreeView(view);
            TestResultsLayout.AddControl(TestResultsTreeView, new Rectangle(0, 0, 9999, 9999));

            _engine = new NefTestEngine();
            _engine.RegisterComponent(typeof(ILogger), typeof(PluginLogger), true);
            _engine.RegisterComponent(typeof(ITestFinder), typeof(PluginTestFinder), true);
            _engine.RegisterComponent(typeof(ITestReporter), typeof(PluginTestReporter), true);
            _engine.Initialize();
            ((PluginTestReporter)_engine.TestReporter).SetView(view, TestResultsTreeView, TestStatusText);
        }
         
        private void RunTestsButton_Hit(object sender, EventArgs e) {
            try {
                var filter = new TestFilter() {
                    Environment = EnvironmentType.Any
                };

                var testResult = _engine.TestRunner.RunAvailableTests(filter);
                _engine.TestReporter.Write(testResult);
            }
            catch (Exception ex) {
                CoreManager.Current.Actions.AddChatText($"Error running tests: {ex}", 5);
            }
        }

        private void FindTestsButton_Hit(object sender, EventArgs e) {
            try {
                var filter = new TestFilter() {
                    Environment = EnvironmentType.Any
                };

                var testResult = _engine.TestRunner.FindAvailableTests(filter);
                _engine.TestReporter.Write(testResult);
            }
            catch (Exception ex) {
                CoreManager.Current.Actions.AddChatText($"Error finding tests: {ex}", 5);
            }
        }
        #endregion

        #region Logging
        public void WriteLog(string message) {
            try {
                using (StreamWriter writer = new StreamWriter(Path.Combine(PluginAssemblyDirectory, "exceptions.txt"), true)) {
                    writer.WriteLine($"NefTest.PluginRunner: {message}");
                    writer.Close();
                }
            }
            catch { }
        }
        #endregion
    }
}
