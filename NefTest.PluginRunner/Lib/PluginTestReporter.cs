﻿using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using VirindiViewService;
using VirindiViewService.Controls;
using static NefTest.PluginRunner.Lib.KeyValueTreeView;

namespace NefTest.PluginRunner.Lib {
    public class PluginTestReporter : ITestReporter {
        private ILogger _logger;

        private HudView _view;
        private KeyValueTreeView _resultsTree;
        private HudStaticText _statusText;

        public PluginTestReporter(ILogger logger) {
            _logger = logger;
        }

        public void Write(ITestResult testResult) {
            WriteToLogger(testResult);
            if (_view != null && _resultsTree != null && _statusText != null)
                WriteToUI(testResult);
        }

        private void WriteToUI(ITestResult testResult) {
            int totalFailing = testResult.TotalCasesByStatus(TestStatus.Failed);
            int totalPassing = testResult.TotalCasesByStatus(TestStatus.Passed);
            int totalNotRun = testResult.TotalCasesByStatus(TestStatus.NotRun);
            int totalSkipped = testResult.TotalCasesByStatus(TestStatus.Skipped);

            _statusText.Text = $"{totalFailing} failing, {totalPassing} passing, {totalNotRun} not run, {totalSkipped} skipped, {testResult.TotalCases()} total ({testResult.Duration.TotalMilliseconds:N3}ms)";
            _statusText.TextColor = ColorStatus(testResult.Status);

            _resultsTree.Clear();

            var treeItems = new List<TreeItem>();

            foreach (var testAssembly in testResult.GetAssemblyResults()) {
                int assemblyFailing = testAssembly.TotalCasesByStatus(TestStatus.Failed);
                int assemblyPassing = testAssembly.TotalCasesByStatus(TestStatus.Passed);
                int assemblyNotRun = testAssembly.TotalCasesByStatus(TestStatus.NotRun);
                int assemblySkipped = testAssembly.TotalCasesByStatus(TestStatus.Skipped);
                var assemblyKey = $"{testAssembly.ShortName}: {assemblyFailing} failing, {assemblyPassing} passing, {assemblyNotRun} not run, {assemblySkipped} skipped, {testAssembly.TotalCases()} total ({testAssembly.Duration.TotalMilliseconds:N3}ms)";

                var assemblyTreeItem = new TreeItem(assemblyKey, testAssembly.Status.ToString(), null, "", ColorStatus(testAssembly.Status).ToArgb(), ColorStatus(testAssembly.Status).ToArgb());
                treeItems.Add(assemblyTreeItem);

                foreach (var testClass in testAssembly.GetClassResults()) {
                    int testClassFailing = testClass.TotalCasesByStatus(TestStatus.Failed);
                    int testClassPassing = testClass.TotalCasesByStatus(TestStatus.Passed);
                    int testClassNotRun = testClass.TotalCasesByStatus(TestStatus.NotRun);
                    int testClassSkipped = testClass.TotalCasesByStatus(TestStatus.Skipped);
                    var testClassKey = $"{testClass.ShortName}: {testClassFailing} failing, {testClassPassing} passing, {testClassNotRun} not run, {testClassSkipped} skipped, {testClass.TotalCases()} total ({testClass.Duration.TotalMilliseconds:N3}ms)";

                    var testClassTreeItem = new TreeItem(testClassKey, testClass.Status.ToString(), assemblyTreeItem, "", ColorStatus(testClass.Status).ToArgb(), ColorStatus(testClass.Status).ToArgb());
                    assemblyTreeItem.AddChild(testClassTreeItem);

                    foreach (var testCase in testClass.GetCaseResults()) {
                        var output = string.IsNullOrEmpty(testCase.Output) ? "" : $": {testCase.Output}";
                        var testCaseKey = $"{testCase.ShortName}: {testCase.Status}{output} ({testCase.Duration.TotalMilliseconds:N3}ms)";
                        var testCaseTreeItem = new TreeItem(testCaseKey, testCase.Status.ToString(), testClassTreeItem, "", ColorStatus(testCase.Status).ToArgb(), ColorStatus(testCase.Status).ToArgb());
                        testClassTreeItem.AddChild(testCaseTreeItem);
                    }
                }
            }

            _resultsTree.AddTreeItems(treeItems);
            _resultsTree.ExpandAll();
        }

        private void WriteToLogger(ITestResult testResult) {
            int totalFailing = testResult.TotalCasesByStatus(TestStatus.Failed);
            int totalPassing = testResult.TotalCasesByStatus(TestStatus.Passed);
            int totalNotRun = testResult.TotalCasesByStatus(TestStatus.NotRun);
            int total = testResult.GetAssemblyResults().Sum(a => a.GetClassResults().Sum(c => c.GetCaseResults().Count()));

            _logger.Log($"Test Results: {totalFailing} failing, {totalPassing} passing, {totalNotRun} not run, {total} total ({testResult.Duration.TotalMilliseconds:N3}ms)");

            foreach (var testAssembly in testResult.GetAssemblyResults()) {
                int assemblyFailing = testAssembly.TotalCasesByStatus(TestStatus.Failed);
                int assemblyPassing = testAssembly.TotalCasesByStatus(TestStatus.Passed);
                int assemblyNotRun = testAssembly.TotalCasesByStatus(TestStatus.NotRun);
                _logger.Log($"  - {testAssembly.ShortName}: {assemblyFailing} failing, {assemblyPassing} passing, {assemblyNotRun} not run ({testAssembly.Duration.TotalMilliseconds:N3}ms)");

                foreach (var testClass in testAssembly.GetClassResults()) {
                    int testClassFailing = testClass.TotalCasesByStatus(TestStatus.Failed);
                    int testClassPassing = testClass.TotalCasesByStatus(TestStatus.Passed);
                    int testClassNotRun = testClass.TotalCasesByStatus(TestStatus.NotRun);
                    _logger.Log($"    - {testClass.ShortName}: {testClassFailing} failing, {testClassPassing} passing, {testClassNotRun} not run ({testClass.Duration.TotalMilliseconds:N3}ms)");

                    foreach (var testCase in testClass.GetCaseResults()) {
                        var output = string.IsNullOrEmpty(testCase.Output) ? "" : $": {testCase.Output}";
                        _logger.Log($"      - {testCase.ShortName}: {testCase.Status}{output} ({testCase.Duration.TotalMilliseconds:N3}ms)");
                    }
                }
            }
        }

        internal void SetView(HudView view, KeyValueTreeView resultsList, HudStaticText statusText) {
            _view = view;
            _resultsTree = resultsList;
            _statusText = statusText;
        }

        private Color ColorStatus(TestStatus status) {
            switch (status) {
                case TestStatus.Passed:
                    return Color.Green;
                case TestStatus.Failed:
                    return Color.Red;
                case TestStatus.NotRun:
                    return Color.Gray;
                case TestStatus.Skipped:
                    return Color.Yellow;
                default:
                    return Color.LightBlue;
            }
        }
    }
}
