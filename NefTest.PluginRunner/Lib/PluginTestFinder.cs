﻿using NefTest.Engine;
using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NefTest.PluginRunner.Lib {
    public class PluginTestFinder : ITestFinder {
        private readonly List<string> _checkedDirectories = new List<string>();
        private readonly List<string> _testAssemblyPaths = new List<string>();
        private readonly ILogger _log;
        private readonly IEngine _engine;

        public PluginTestFinder(ILogger logger, IEngine engine) {
            _log = logger;
            _engine = engine;
        }

        public IEnumerable<ITestAssembly> FindAllAvailableTestAssemblies() {
            var testAssemblies = new List<ITestAssembly>();
            var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();

            _testAssemblyPaths.Clear();
            _checkedDirectories.Clear();

            foreach (var assembly in loadedAssemblies) {
                try {
                    if (assembly.GlobalAssemblyCache || assembly.ManifestModule is System.Reflection.Emit.ModuleBuilder || string.IsNullOrEmpty(assembly.Location))
                        continue;

                    var assemblyDir = Path.GetDirectoryName(assembly.Location);
                    if (_checkedDirectories.Contains(assemblyDir.ToLower()))
                        continue;

                    _checkedDirectories.Add(assemblyDir.ToLower());

                    var files = Directory.GetFiles(assemblyDir, "*.Tests.dll").ToList();
                    if (Directory.Exists(Path.Combine(assemblyDir, "tests"))) {
                        files.AddRange(Directory.GetFiles(Path.Combine(assemblyDir, "tests"), "*.Tests.dll"));
                    }

                    foreach (var file in files) {
                        if (!_testAssemblyPaths.Contains(file)) {
                            _log.Log($"Found test assembly: {file}");
                            _testAssemblyPaths.Add(file);

                            testAssemblies.Add(_engine.Resolve<ITestAssembly>(new Dictionary<object, object>() {
                                { "assemblyPath", file }
                            }));
                        }
                    }
                }
                catch (Exception ex) {
                    _log.Log($"Error checking loaded assemblies for plugin tests:", LogLevel.Error);
                    _log.Log(ex);
                }
            }

            return testAssemblies;
        }

        public ITestAssembly GetTestAssembly(string assemblyPath) {
            throw new NotImplementedException();
        }

        public ITestCase GetTestCase(string testCaseName, string assemblyPath) {
            throw new NotImplementedException();
        }
    }
}
