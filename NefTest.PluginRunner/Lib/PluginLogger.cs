﻿using Decal.Adapter;
using NefTest.Engine;
using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NefTest.PluginRunner.Lib {
    public class PluginLogger : ILogger {
        public void Log(string message, LogLevel logLevel = LogLevel.Info) {
            try {
                CoreManager.Current.Actions.AddChatText(message.Replace("\r", ""), GetLogLevelGameColor(logLevel));
            }
            catch { }
        }

        private int GetLogLevelGameColor(LogLevel logLevel) {
            switch (logLevel) {
                case LogLevel.Verbose:
                    return 2;
                case LogLevel.Info:
                    return 13;
                case LogLevel.Success:
                    return 24;
                case LogLevel.Warning:
                    return 31;
                case LogLevel.Error:
                    return 21;
                default:
                    return 0;
            }
        }

        public void Log(Exception ex) {
            if (ex is TargetInvocationException)
                ex = ex.InnerException;
            Log(ex.ToString(), LogLevel.Error);
        }
    }
}
