﻿using Microsoft.VisualStudio.TestPlatform.ObjectModel.Client;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Logging;
using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.IO;

namespace NefTest.VisualStudio.TestAdapter {
    public class TestLogger : ITestLogger, ILogger {
        private IMessageLogger messageLogger;
        private string _logfile = @"C:\Games\testadapter.txt";
        
        public TestLogger(IMessageLogger messageLogger) {
            this.messageLogger = messageLogger;
        }

        public void Initialize(TestLoggerEvents events, string testRunDirectory) {

        }

        private void _doLog(string v) {
            //if (!File.Exists(_logfile))
            //    File.Create(_logfile);
            //File.AppendAllText(_logfile, $"{v}\r\n");
        }

        internal void Debug(string v) {
            _doLog($"{v}\r\n");
        }

        internal void Warning(string v, Exception ex) {
            _doLog($"{v}: {ex}\r\n");
        }

        internal void Info(string v) {
            _doLog($"{v}\r\n");
        }

        internal void Error(string v) {
            _doLog($"{v}\r\n");
        }

        internal void Warning(string v) {
            _doLog($"{v}\r\n");
        }

        public void Log(string message, LogLevel logLevel = LogLevel.Info) {
            _doLog($"{message}\r\n");
        }

        public void Log(Exception ex) {
            _doLog($"{ex}\r\n");
        }
    }
}