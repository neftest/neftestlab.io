﻿using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Microsoft.VisualStudio.TestPlatform.ObjectModel.Adapter;
using NefTest.Engine.Enums;
using NefTest.Engine.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NefTest.VisualStudio.TestAdapter {
    public class AdapterTestExecutorReporter : ITestReporter {
        private readonly ILogger _logger;
        private IFrameworkHandle _frameworkHandle;
        private string _executorUri;

        public AdapterTestExecutorReporter(ILogger logger) {
            _logger = logger;
        }

        public void SetFrameworkHandle(IFrameworkHandle frameworkHandle, string executorUri) {
            _frameworkHandle = frameworkHandle;
            _executorUri = executorUri;
        }

        public void Write(ITestResult testResult) {
            if (_frameworkHandle == null) {
                _logger.Log($"AdapterTestReporter._frameworkHandle was null! Make sure to set it first with AdapterTestReporter.SetFrameworkHandler(frameworkHandle)");
                return;
            }

            foreach (var testAssembly in testResult.GetAssemblyResults()) {
                foreach (var testClass in testAssembly.GetClassResults()) {
                    foreach (var testCase in testClass.GetCaseResults()) {
                        var tc = new TestCase(testCase.FullyQualifiedName, new Uri(_executorUri), testAssembly.TestAssembly.AssemblyPath);
                        
                        TestOutcome outcome;
                        switch (testCase.Status) {
                            case TestStatus.Passed:
                                outcome = TestOutcome.Passed;
                                break;
                            case TestStatus.Failed:
                                outcome = TestOutcome.Failed;
                                break;
                            case TestStatus.Skipped:
                                outcome = TestOutcome.Skipped;
                                break;
                            default:
                                outcome = TestOutcome.None;
                                break;
                        }

                        if (testCase.TestCase.Environment != Common.Enums.EnvironmentType.Any)
                            tc.Traits.Add("Env", testCase.TestCase.Environment.ToString());

                        _frameworkHandle.RecordResult(new TestResult(tc) {
                            Outcome = outcome,
                            ErrorMessage = (outcome == TestOutcome.Failed || outcome == TestOutcome.Skipped) ? testCase.Output : null,
                            Duration = testCase.Duration
                        });
                    }
                }
            }
        }
    }
}
